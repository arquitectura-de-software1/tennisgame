
public class TennisGame2 implements TennisGame
{
    public int player1Point = 0;
    public int player2Point = 0;
    
    public String player1Result = "";
    public String player2Result = ""; 
    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore(){
        String score = "";
        score = tie(score);
        score = deuce(score);

        score = normal(score);
        score = normal1(score);
        score = normal2(score);
        score = normal3(score);
        
        score = advantage(score);
        
        score = win(score);
        return score;
    }

	private String normal3(String score) {
		if (player2Point>player1Point && player2Point < 4)
        {
            score = getResultPoints();
        }
		return score;
	}

	private String normal2(String score) {
		if (player1Point>player2Point && player1Point < 4)
        {  
            score = getResultPoints();
        }
		return score;
	}

	private String normal1(String score) {
		if (player2Point > 0 && player1Point==0)
        {
            score = getResultPoints();
        }
		return score;
	}

	private String normal(String score) {
		if (player1Point > 0 && player2Point==0)
        {
			score = getResultPoints();
        }
		return score;
	}
	
	private String getResultPoints() {
        String score;
        player1Result = getLiteral(player1Point);
        player2Result = getLiteral(player2Point);
		score = player1Result + "-" + player2Result;
		return score;
	}

	private String getLiteral(int playerPoint) {
		String result="";
		if (playerPoint==0)
			result = "Love";
		if (playerPoint==1)
			result = "Fifteen";
		if (playerPoint==2)
			result = "Thirty";
		if (playerPoint==3)
			result = "Forty";
		return result;
	}

	private String deuce(String score) {
		if (player1Point==player2Point && player1Point>=3)
            score = "Deuce";
		return score;
	}

	private String tie(String score) {
		if (player1Point == player2Point && player1Point < 4)
        {
            if (player1Point==0)
                score = "Love";
            if (player1Point==1)
                score = "Fifteen";
            if (player1Point==2)
                score = "Thirty";
            score += "-All";
        }
		return score;
	}

	private String win(String score) {
		if (player1Point>=4 && player2Point>=0 && (player1Point-player2Point)>=2)
        {
            score = "Win for player1";
        }
        if (player2Point>=4 && player1Point>=0 && (player2Point-player1Point)>=2)
        {
            score = "Win for player2";
        }
		return score;
	}

	private String advantage(String score) {
		if (player1Point > player2Point && player2Point >= 3)
        {
            score = "Advantage player1";
        }
        if (player2Point > player1Point && player1Point >= 3)
        {
            score = "Advantage player2";
        }
		return score;
	}
    
    public void SetP1Score(int number){
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }     
    }
    
    public void SetP2Score(int number){
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }     
    }
    
    public void P1Score(){
    	player1Point++;
    }
    
    public void P2Score(){
    	player2Point++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}